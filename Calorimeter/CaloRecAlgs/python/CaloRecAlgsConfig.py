""" Define methods used to instantiate configured Calorimeter Calibration reconstruction tools and algorithms

Copyright (C) 2022 CERN for the benefit of the FASER collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from IOVDbSvc.IOVDbSvcConfig import addFolders
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

from CaloRecTools.CaloRecToolConfig import CaloRecToolCfg

# One stop shopping for normal FASER data
def CalorimeterReconstructionCfg(flags, **kwargs):
    """ Return all algorithms and tools for Waveform reconstruction """
    acc = ComponentAccumulator()

    kwargs.setdefault("CaloWaveHitContainerKey", "CaloWaveformHits")
    kwargs.setdefault("PreshowerWaveHitContainerKey", "PreshowerWaveformHits")
    kwargs.setdefault("CaloHitContainerKey", "CaloHits")
    kwargs.setdefault("PreshowerHitContainerKey", "PreshowerHits")

    recoAlg = CompFactory.CaloRecAlg("CaloRecAlg", **kwargs)
    acc.addEventAlgo(recoAlg)

    dbInstance = kwargs.get("dbInstance", "TRIGGER_OFL") # what should this be set to???

    acc.merge(CaloRecToolCfg(flags))

    return acc

def CalorimeterReconstructionOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Calorimeter Reco"""
    acc = ComponentAccumulator()
    ItemList = [
        "xAOD::CalorimeterHitContainer#*"
        , "xAOD::CalorimeterHitAuxContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "xAOD", ItemList))
    # ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Don't know what this does
    return acc

