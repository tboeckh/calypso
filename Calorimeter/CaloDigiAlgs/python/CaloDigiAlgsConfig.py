# Copyright (C) 2020-2021 CERN for the benefit of the FASER collaboration

from re import VERBOSE
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg

from WaveformConditionsTools.WaveformCableMappingConfig import WaveformCableMappingCfg

# One stop shopping for normal FASER data
def CaloWaveformDigitizationCfg(flags, **kwargs):
    """ Return all algorithms and tools for Waveform digitization """
    acc = ComponentAccumulator()

    if not flags.Input.isMC:
        return acc

    acc.merge(CaloWaveformDigiCfg(flags, "CaloWaveformDigiAlg", **kwargs))
    acc.merge(CaloWaveformDigitizationOutputCfg(flags))
    acc.merge(WaveformCableMappingCfg(flags))

    return acc

# Return configured digitization algorithm from SIM hits
def CaloWaveformDigiCfg(flags, name="CaloWaveformDigiAlg", **kwargs):

    acc = ComponentAccumulator()

    tool = CompFactory.WaveformDigitisationTool(name="CaloWaveformDigtisationTool")
    kwargs.setdefault("WaveformDigitisationTool", tool)
    
    kwargs.setdefault("CaloHitContainerKey", "EcalHits")
    kwargs.setdefault("WaveformContainerKey", "CaloWaveforms")

    kwargs.setdefault("CB_alpha", -0.32)
    kwargs.setdefault("CB_n", 1000)
    kwargs.setdefault("CB_sigma", 3.67)
    kwargs.setdefault("CB_mean", 820) # Time in ns
    # This number is over-ridden in the digitization script, so change it there!
    kwargs.setdefault("CB_norm", 5.0)  # Low gain default without filters, use x5? for high gain

    kwargs.setdefault("base_mean", 15650)
    kwargs.setdefault("base_rms", 3)

    digiAlg = CompFactory.CaloWaveformDigiAlg(name, **kwargs)
    print(f"CaloWaveformDigiAlg normalization: {digiAlg.CB_norm}")
    print(f"CaloWaveformDigiAlg mean time: {digiAlg.CB_mean}")

    acc.addEventAlgo(digiAlg)

    return acc

def CaloWaveformDigitizationOutputCfg(flags, **kwargs):
    """ Return ComponentAccumulator with output for Waveform Digi"""
    acc = ComponentAccumulator()
    ItemList = [
        "RawWaveformContainer#*"
    ]
    acc.merge(OutputStreamCfg(flags, "RDO"))
    ostream = acc.getEventAlgo("OutputStreamRDO")
    # ostream.TakeItemsFromInput = True # Copies all data from input file to output
    # ostream.TakeItemsFromInput = False
    # Try turning this off
    ostream.ItemList += ItemList
    return acc
