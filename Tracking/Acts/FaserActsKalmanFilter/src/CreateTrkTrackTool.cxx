#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "CreateTrkTrackTool.h"

#include <Acts/EventData/MultiTrajectoryHelpers.hpp>
#include "TrackerIdentifier/FaserSCT_ID.h"

CreateTrkTrackTool::CreateTrkTrackTool(const std::string& type, const std::string& name, const IInterface* parent) :
    AthAlgTool( type, name, parent ) {}

StatusCode CreateTrkTrackTool::initialize() {
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
  return StatusCode::SUCCESS;
}

StatusCode CreateTrkTrackTool::finalize() {
  return StatusCode::SUCCESS;
}

std::unique_ptr<Trk::Track>
CreateTrkTrackTool::createTrack(const Acts::GeometryContext &gctx, const FaserActsRecMultiTrajectory &traj) const {
  std::unique_ptr<Trk::Track> newtrack = nullptr;
  DataVector<const Trk::TrackStateOnSurface>* finalTrajectory = new DataVector<const Trk::TrackStateOnSurface>{};
  using ConstTrackStateProxy = Acts::detail_lt::TrackStateProxy<IndexSourceLink, 6, true>;
  auto trajState = Acts::MultiTrajectoryHelpers::trajectoryState(traj.multiTrajectory(), traj.tips().front());
  traj.multiTrajectory().visitBackwards(traj.tips().front(), [&](const ConstTrackStateProxy& state) {
    auto flag = state.typeFlags();
    if (state.referenceSurface().associatedDetectorElement() != nullptr) {
      std::bitset<Trk::TrackStateOnSurface::NumberOfTrackStateOnSurfaceTypes> typePattern;
      const Trk::TrackParameters *parm;

      if (flag[Acts::TrackStateFlag::HoleFlag]) {
        const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                   state.predicted(), state.predictedCovariance());
        parm = ConvertActsTrackParameterToATLAS(actsParam, gctx);
        typePattern.set(Trk::TrackStateOnSurface::Hole);
      }
      else if (flag[Acts::TrackStateFlag::OutlierFlag]) {
        const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                   state.filtered(), state.filteredCovariance());
        parm = ConvertActsTrackParameterToATLAS(actsParam, gctx);
        typePattern.set(Trk::TrackStateOnSurface::Outlier);
      }
      else {
        const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                   state.smoothed(), state.smoothedCovariance());
        parm = ConvertActsTrackParameterToATLAS(actsParam, gctx);
        typePattern.set(Trk::TrackStateOnSurface::Measurement);
      }
      Tracker::FaserSCT_ClusterOnTrack* clusterOnTrack = nullptr;
      if (state.hasUncalibrated()) {
        const Tracker::FaserSCT_Cluster* cluster = state.uncalibrated().hit();
        if (cluster->detectorElement() != nullptr) {
          clusterOnTrack = new Tracker::FaserSCT_ClusterOnTrack{
              cluster,
              Trk::LocalParameters{
                  Trk::DefinedParameter{cluster->localPosition()[0], Trk::loc1},
                  Trk::DefinedParameter{cluster->localPosition()[1], Trk::loc2}
              },
              cluster->localCovariance(),
              m_idHelper->wafer_hash(cluster->detectorElement()->identify())
          };
        }
      }
      const Trk::TrackStateOnSurface *perState = new Trk::TrackStateOnSurface(clusterOnTrack, parm);
      if (perState) {
        finalTrajectory->insert(finalTrajectory->begin(), perState);
      }
    }
    return;
  });

  Trk::FitQuality* q = new Trk::FitQuality {trajState.chi2Sum, static_cast<double>(trajState.nMeasurements - 5)};
  Trk::TrackInfo newInfo(Trk::TrackInfo::TrackFitter::KalmanFitter, Trk::ParticleHypothesis::muon);
  std::unique_ptr<DataVector<const Trk::TrackStateOnSurface>> sink(finalTrajectory);
  // newtrack = std::make_unique<Trk::Track>(newInfo, std::move(*finalTrajectory), quality);
  Trk::Track* tmpTrack = new Trk::Track(newInfo, std::move(*sink), q);
  newtrack = std::unique_ptr<Trk::Track>(tmpTrack);
  return newtrack;
}


const Trk::TrackParameters*
CreateTrkTrackTool::ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const {
  using namespace Acts::UnitLiterals;
  std::optional<AmgSymMatrix(5)> cov = std::nullopt;
  if (actsParameter.covariance()){
    AmgSymMatrix(5) newcov(actsParameter.covariance()->topLeftCorner(5, 5));
    // Convert the covariance matrix to GeV
    for(int i=0; i < newcov.rows(); i++){
      newcov(i, 4) = newcov(i, 4)*1_MeV;
    }
    for(int i=0; i < newcov.cols(); i++){
      newcov(4, i) = newcov(4, i)*1_MeV;
    }
    cov =  std::optional<AmgSymMatrix(5)>(newcov);
  }
  const Amg::Vector3D& pos=actsParameter.position(gctx);
  double tphi=actsParameter.get<Acts::eBoundPhi>();
  double ttheta=actsParameter.get<Acts::eBoundTheta>();
  double tqOverP=actsParameter.get<Acts::eBoundQOverP>()*1_MeV;
  double p = std::abs(1. / tqOverP);
  Amg::Vector3D tmom(p * std::cos(tphi) * std::sin(ttheta), p * std::sin(tphi) * std::sin(ttheta), p * std::cos(ttheta));
  double charge = tqOverP > 0. ? 1. : -1.;
  const Trk::CurvilinearParameters * curv = new Trk::CurvilinearParameters(pos,tmom,charge, cov);
  return curv;
}
