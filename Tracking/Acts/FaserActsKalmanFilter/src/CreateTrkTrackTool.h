#ifndef FASERACTSKALMANFILTER_CREATETRKTRACKTOOL_H
#define FASERACTSKALMANFILTER_CREATETRKTRACKTOOL_H

#include "TrackerPrepRawData/FaserSCT_ClusterContainer.h"
#include "AthenaBaseComps/AthAlgTool.h"
#include "TrkTrack/Track.h"
#include "TrkParameters/TrackParameters.h"
#include "Acts/Geometry/GeometryContext.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsRecMultiTrajectory.h"


class FaserSCT_ID;

class CreateTrkTrackTool: public AthAlgTool {
public:
  CreateTrkTrackTool(const std::string &type, const std::string &name, const IInterface *parent);
  virtual ~CreateTrkTrackTool() = default;
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  std::unique_ptr<Trk::Track> createTrack(const Acts::GeometryContext &gctx, const FaserActsRecMultiTrajectory &traj) const;
  const Trk::TrackParameters* ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const;
private:
  const FaserSCT_ID* m_idHelper {nullptr};
};

#endif //FASERACTSKALMANFILTER_CREATETRKTRACKTOOL_H