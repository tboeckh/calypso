def getNEvents(fname, maxEvents):
    "Work out how many events are in the file"

    n = 0
    with open(fname) as f:
        n = sum(1 for l in f if l.startswith("E "))

    if maxEvents != -1 and n > maxEvents:
        n = maxEvents

    print ("Setting Maximum events to", n)
    return n


if __name__ == "__main__":

    import sys
    
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG
    log.setLevel(DEBUG)
    
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior = 1

    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Input.RunNumber = [12345]
    ConfigFlags.Input.OverrideRunNumber = True
    ConfigFlags.Input.LumiBlockNumber = [1]

    ConfigFlags.Input.isMC = True
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"             # Always needed; must match FaserVersion
    ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # Default FASER geometry
    ConfigFlags.Detector.EnableFaserSCT = True

    ConfigFlags.Output.EVNTFileName = "myEVNT.pool.root"

    ConfigFlags.Exec.MaxEvents= -1

    import sys
    ConfigFlags.fillFromArgs(sys.argv[1:])


    ConfigFlags.Exec.MaxEvents = getNEvents(ConfigFlags.Input.Files[0], ConfigFlags.Exec.MaxEvents)    

    ConfigFlags.lock()

    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    cfg = MainServicesCfg(ConfigFlags)

    from HEPMCReader.HepMCReaderConfig import HepMCReaderCfg
    cfg.merge(HepMCReaderCfg(ConfigFlags))

    from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
    cfg.merge(McEventSelectorCfg(ConfigFlags))    

    itemList = [ "EventInfo#McEventInfo", "McEventCollection#*" ]
    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    cfg.merge(OutputStreamCfg(ConfigFlags, "EVNT", itemList, disableEventTag = True))

    sc = cfg.run()
    sys.exit(not sc.isSuccess())
