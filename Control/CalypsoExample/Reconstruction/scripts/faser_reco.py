#!/usr/bin/env python
#
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
# Run with:
# ./faser_reco.py [--geom=runtype] filepath 
# 
# filepath - fully qualified path, including url if needed, to the input raw data file
#   example: "root://hepatl30//atlas/local/torrence/faser/commissioning/TestBeamData/Run-004150/Faser-Physics-004150-00000.raw"
# 
# runtype - optionally specify the data type (TI12Data, TI12Data02, TI12Data03 or TestBeamData).
#
# Options:
#   --isMC - needed to reconstruct MC data
#   --testBeam - shortcut to specify testbeam geometry
#
import sys
import time
import argparse

a = time.time()

parser = argparse.ArgumentParser(description="Run FASER reconstruction")

parser.add_argument("file_path",
                    help="Fully qualified path of the raw input file")
parser.add_argument("-g", "--geom", default="",
                    help="Specify geometry (if it can't be parsed from run number)\n Values: TI12Data03 (2022 TI12)")
parser.add_argument("-r", "--reco", default="",
                    help="Specify reco tag (to append to output filename)")
parser.add_argument("-n", "--nevents", type=int, default=-1,
                    help="Specify number of events to process (default: all)")
parser.add_argument("-v", "--verbose", action='store_true', 
                    help="Turn on DEBUG output")
parser.add_argument("--isMC", action='store_true',
                    help="Running on digitised MC rather than data")
parser.add_argument("--testBeam", action='store_true',
                    help="Set geometry for 2021 test beam")

args = parser.parse_args()

from pathlib import Path

filepath=Path(args.file_path)

# runtype has been provided
if len(args.geom) > 0:
    runtype=args.geom

# Shortcut for testbeam 
elif args.testBeam:
    print(f"Use 2021 TestBeam configuration")
    runtype = "TestBeamData"

else:
    print(f"Assuming TI12 geometry")
    runtype = "TI12Data03"

    # Try to pick correct geometry from run number
    # This won't work for testbeam data, 
    # so lets call this a hack for now
    runname = filepath.parts[-1]
    try:
        runnumber = int(runname.split('-')[2])
    except Exception as e:
        print(f"Failed to find run number in {filepath}")
        print(f"Couldn't parse {runname}")
        print(f"Leave runtype as {runtype}!")
    else:
        if runnumber > 6700: # Not sure if this is exact
            print(f"Found run number {runnumber}, using TI12 configuration with IFT+faserNu")
            runtype = "TI12Data03"
        elif runnumber > 5302: # Last TI12 run on Nov. 23, 2021 without IFT
            print(f"Found run number {runnumber}, using TI12 configuration with IFT")
            runtype = "TI12Data02"
        else:
            print(f"Found run number {runnumber}, using original TI12 configuration")
            runtype = "TI12Data"

print(f"Starting reconstruction of {filepath.name} with type {runtype}")
if args.nevents > 0:
    print(f"Reconstructing {args.nevents} events by command-line option")

# Start reconstruction

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaCommon.Constants import VERBOSE, INFO

from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags

Configurable.configurableRun3Behavior = True
    
# Flags for this job
ConfigFlags.Input.isMC = args.isMC               # Needed to bypass autoconfig
if args.isMC:
    ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"   # Use MC conditions
else:
    ConfigFlags.IOVDb.DatabaseInstance = "CONDBR3" # Use data conditions

ConfigFlags.Input.ProjectName = "data20"
ConfigFlags.GeoModel.Align.Dynamic    = False

# Flags for later
useCKF = True
useCal = False
useLHC = False

# Enable ACTS material corrections, this crashes testbeam geometries
ConfigFlags.TrackingGeometry.MaterialSource = "/cvmfs/faser.cern.ch/repo/sw/database/DBRelease/current/acts/material-maps.json"

# TI12 Cosmics geometry
if runtype == "TI12Data":
    ConfigFlags.GeoModel.FaserVersion = "FASER-01" 
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-01"

# Testbeam setup 
elif runtype == "TestBeamData" or runtype == "TestBeamMC":
    ConfigFlags.GeoModel.FaserVersion = "FASER-TB00" 
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-TB00"
    useCKF = False
    useCal = True

# New TI12 geometry (ugh)
elif runtype == "TI12Data02":
    ConfigFlags.GeoModel.FaserVersion = "FASER-02" 
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"

# Final 2022 TI12 geometry
elif runtype == "TI12Data03":
    ConfigFlags.GeoModel.FaserVersion = "FASERNU-03" 
    # ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-02"
    # Use the updated field map
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"
    useCal = True
    useLHC = True

else:
    print("Invalid run type found:", runtype)
    print("Specify correct type or update list")
    sys.exit(1)


# Must use original input string here, as pathlib mangles double // in path names
ConfigFlags.Input.Files = [ args.file_path ]

filestem = filepath.stem
# Remove any filetype modifier
if filestem[-4:] == "-RDO":
    filestem = filestem[:-4]

if len(args.reco) > 0:
    filestem += f"-{args.reco}"

ConfigFlags.addFlag("Output.xAODFileName", f"{filestem}-xAOD.root")
ConfigFlags.Output.ESDFileName = f"{filestem}-ESD.root"
ConfigFlags.Output.doWriteESD = False

#
# Play around with this?
# ConfigFlags.Concurrency.NumThreads = 2
# ConfigFlags.Concurrency.NumConcurrentEvents = 2
ConfigFlags.lock()

#
# Configure components
from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    
acc = MainServicesCfg(ConfigFlags)
acc.merge(PoolWriteCfg(ConfigFlags))

#
# Set up RAW data access

if args.isMC:
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(ConfigFlags))
else:    
    from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
    acc.merge(FaserByteStreamCnvSvcCfg(ConfigFlags))

#
# Needed, or move to MainServicesCfg?
from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
acc.merge(FaserGeometryCfg(ConfigFlags))

if useLHC:
    from LHCDataAlgs.LHCDataAlgConfig import LHCDataAlgCfg
    acc.merge(LHCDataAlgCfg(ConfigFlags))

# Set up algorithms
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg    
acc.merge(WaveformReconstructionCfg(ConfigFlags))

# Calorimeter reconstruction
if args.isMC:
    # Not ready for MC quite yet
    pass
elif useCal:
    from CaloRecAlgs.CaloRecAlgsConfig import CalorimeterReconstructionCfg
    acc.merge(CalorimeterReconstructionCfg(ConfigFlags))

# Tracker clusters
from TrackerPrepRawDataFormation.TrackerPrepRawDataFormationConfig import FaserSCT_ClusterizationCfg
acc.merge(FaserSCT_ClusterizationCfg(ConfigFlags, DataObjectName="SCT_RDOs"))

#
# SpacePoints
from TrackerSpacePointFormation.TrackerSpacePointFormationConfig import TrackerSpacePointFinderCfg
acc.merge(TrackerSpacePointFinderCfg(ConfigFlags))

# Try Dave's new fitter
from TrackerSegmentFit.TrackerSegmentFitConfig import SegmentFitAlgCfg
acc.merge(SegmentFitAlgCfg(ConfigFlags,
                           SharedHitFraction=0.61, 
                           MinClustersPerFit=5, 
                           TanThetaXZCut=0.083))

# Turn on CKF track finding 
if useCKF:
    # 
    # Ghost removal
    from FaserActsKalmanFilter.GhostBustersConfig import GhostBustersCfg
    acc.merge(GhostBustersCfg(ConfigFlags))

    #
    # Kalman Filter for tracking
    from FaserActsKalmanFilter.CKF2Config import CKF2Cfg
    acc.merge(CKF2Cfg(ConfigFlags, noDiagnostics=True))

    # Add tracking collection with no IFT 
    acc.merge(CKF2Cfg(ConfigFlags, maskedLayers=[0, 1, 2], name="CKF_woIFT", 
                      OutputCollection="CKFTrackCollectionWithoutIFT", noDiagnostics=True))

#
# Configure output
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
itemList = [ "xAOD::EventInfo#*"
             , "xAOD::EventAuxInfo#*"
             , "xAOD::FaserTriggerData#*"
             , "xAOD::FaserTriggerDataAux#*"
             , "FaserSiHitCollection#*"  # Strip hits, do we want this?
             , "FaserSCT_RDO_Container#*" 
             , "FaserSCT_SpacePointContainer#*"
             , "Tracker::FaserSCT_ClusterContainer#*"
             , "TrackCollection#*"
]
#
if useLHC:
    itemList.extend( ["xAOD::FaserLHCData#*", "xAOD::FaserLHCDataAux#*"] )

if args.isMC:
    # Make xAOD versions of truth
    from Reconstruction.xAODTruthCnvAlgConfig import xAODTruthCnvAlgCfg
    acc.merge(xAODTruthCnvAlgCfg(ConfigFlags))

    # Add MC information here
    itemList.extend( ["McEventCollection#*", "TrackerSimDataCollection#*"] )

# Output stream 
acc.merge(OutputStreamCfg(ConfigFlags, "xAOD", itemList, disableEventTag=True))

# Try to turn off annoying INFO message, as we don't use this
# disableEventTag=True doesn't seem to work...
tagBuilder = CompFactory.EventInfoTagBuilder()
tagBuilder.PropagateInput=False
acc.addEventAlgo(tagBuilder)

# Waveform reconstruction output
from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionOutputCfg    
acc.merge(WaveformReconstructionOutputCfg(ConfigFlags))

# Calorimeter reconstruction output
from CaloRecAlgs.CaloRecAlgsConfig import CalorimeterReconstructionOutputCfg
acc.merge(CalorimeterReconstructionOutputCfg(ConfigFlags))

# Check what we have
print( "Writing out xAOD objects:" )
print( acc.getEventAlgo("OutputStreamxAOD").ItemList )

# Hack to avoid problem with our use of MC databases when isMC = False
if not args.isMC:
    replicaSvc = acc.getService("DBReplicaSvc")
    replicaSvc.COOLSQLiteVetoPattern = ""
    replicaSvc.UseCOOLSQLite = True
    replicaSvc.UseCOOLFrontier = False
    replicaSvc.UseGeomSQLite = True

# Configure verbosity    
if args.verbose:
    acc.foreach_component("*").OutputLevel = VERBOSE
    ConfigFlags.dump()
else:
    acc.foreach_component("*").OutputLevel = INFO

    # Reduce event loop printout
    eventLoop = CompFactory.AthenaEventLoopMgr()
    eventLoop.EventPrintoutInterval = 100
    acc.addService(eventLoop)

acc.foreach_component("*ClassID*").OutputLevel = INFO

acc.getService("MessageSvc").Format = "% F%40W%S%7W%R%T %0W%M"

# Execute and finish
sc = acc.run(maxEvents=args.nevents)

b = time.time()
from AthenaCommon.Logging import log
log.info(f"Finish execution in {b-a} seconds")

sys.exit(int(sc.isFailure()))

