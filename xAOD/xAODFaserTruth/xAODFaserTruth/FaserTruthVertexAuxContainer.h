#ifndef XAODFASERTRUTH_TRUTHVERTEXAUXCONTAINER_H
#define XAODFASERTRUTH_TRUTHVERTEXAUXCONTAINER_H

// System include(s):
#include <vector>

// EDM include(s):
#include "AthLinks/ElementLink.h"
#include "xAODCore/AuxContainerBase.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthParticleContainer.h"

namespace xAOD {

  /// Auxiliary store for the truth vertices
  ///
  /// @author Andy Buckley <Andy.Buckey@cern.ch>
  /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
  ///
  /// $Revision: 624338 $
  /// $Date: 2014-10-27 16:08:55 +0100 (Mon, 27 Oct 2014) $
  ///
  class FaserTruthVertexAuxContainer : public AuxContainerBase {

  public:
    /// Default constructor
    FaserTruthVertexAuxContainer();

  private:
    std::vector< int > id;
    std::vector< int > barcode;
    std::vector< std::vector< ElementLink< FaserTruthParticleContainer > > >
    incomingParticleLinks;
    std::vector< std::vector< ElementLink< FaserTruthParticleContainer > > >
    outgoingParticleLinks;
    std::vector< float > x;
    std::vector< float > y;
    std::vector< float > z;
    std::vector< float > t;

  }; // class FaserTruthVertexAuxContainer

} // namespace xAOD

// Declare a CLID for the class
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTruthVertexAuxContainer, 1254974514, 1 )

#endif // XAODFASERTRUTH_TRUTHVERTEXAUXCONTAINER_H
