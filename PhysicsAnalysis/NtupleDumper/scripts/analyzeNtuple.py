#!/usr/bin/env python

# Set up (Py)ROOT.
import ROOT
import glob
import sys
import pandas as pd


# Define a Landau convoluted with a gaussian for MIP fitting
landguas_conv = ROOT.TF1Convolution("landau","gaus",-10,100,True) # the functions have to be close to zero at min and max bin of convolution or else circular Fourier transform will move convolve values at max and min
landguas_conv.SetNofPointsFFT(10000)
landgaus = ROOT.TF1("landgaus",landguas_conv, -10, 100, landguas_conv.GetNpar())
landgaus.SetParNames("Landau constant","Landau MPV","Landau width","Gaussian mean","Gaussian width")

user_input = str(sys.argv[1]) # set to either 'all_high', 'all_low', or a run number

t = ROOT.TChain("nt")
nfiles = 0
all_run_paths = glob.glob("/eos/project/f/faser-commissioning/DeionsNtuples/*")

if user_input=="all_high":
    runconfig = "High_gain"
    print("processing high-gain runs")
    gain = 30.0
    for run_path in all_run_paths:
        nfiles += t.Add(run_path+"/Data-tuple-High_gain*.root") # chain all ntuples from all runs that are high gain
    rootFile = ROOT.TFile("/eos/project/f/faser-commissioning/DeionsNtuples/7930/Data-tuple-High_gain-007930-00000-100.root"); # load file from largest high gain run to get noise histograms
elif user_input=="all_low":
    runconfig = "Low_gain"
    print("processing low-gain runs")
    gain = 1.0
    for run_path in all_run_paths:
        nfiles += t.Add(run_path+"/Data-tuple-Low_gain*.root") # chain all ntuples from all runs that are high gain
    rootFile = ROOT.TFile("/eos/project/f/faser-commissioning/DeionsNtuples/8137/Data-tuple-Low_gain-008137-00000-100.root"); # load file from largest low gain run to get noise histograms
else: # assume user_input is a run number
    # get run configuration from table oon Brian's website
    table_runs = pd.read_html('http://aagaard.web.cern.ch/aagaard/FASERruns.html') # load in run tables from website
    df = table_runs[0] # table_runs is a list of all tables on the website, we only want the first one
    runconfig=str(df.at[df.loc[df['Run'] == int(user_input)].index[0],'Configuration'].replace(' ','_')) # get config from website run log telling if run is High_gain or Low_gain calo 
    print("processing run "+runconfig+" ("+runconfig+")")
    if runconfig=="High_gain":
        gain = 30.0
    elif runconfig=="Low_gain":
        gain = 1.0
    else:
        print("run config is neither 'High_gain' nor 'Low_gain', calo histogram ranges may be messed up")
        gain = 1.0 # assume low gain

    nfiles += t.Add("/eos/project/f/faser-commissioning/DeionsNtuples/"+user_input+"/*.root") # chain all ntuples from all runs that are high gain
    rootFile = ROOT.TFile("/eos/project/f/faser-commissioning/DeionsNtuples/"+user_input+"/Data-tuple-"+runconfig+"-00"+user_input+"-00000-100.root"); # load file from largest low gain run to get noise histograms




print("number of files chained together = ",nfiles)

#ROOT.gROOT.SetStyle("ATLAS")
#ROOT.gStyle.SetOptStat(111110) #take away option box in histograms
#ROOT.gStyle.SetOptTitle(1)
#ROOT.gStyle.SetOptFit(1)

# Define histograms here
hCaloCharge = []
hCaloPeak = []
hXYvsEcalo = []
for chan in range(4):
    hCaloCharge.append(ROOT.TH1F("hCalo"+str(chan)+"charge", "Charge in calo ch"+str(chan)+";Q (pC);# of events",100,0.2*gain,2.0*gain))
    hCaloPeak.append(ROOT.TH1F("hCalo"+str(chan)+"peak", "Peak in calo ch"+str(chan)+";peak (mV);# of events",100,1.0*gain,5.0*gain))
    hXYvsEcalo.append(ROOT.TProfile2D("hXYvsEcalo"+str(chan)+"" , "Calo ch"+str(chan)+" Charge vs Pos;X pos (mm);Y pos (mm)",26, -130.0, 130.0, 26, -130.0, 130.0))

hCaloChargeTotal = ROOT.TH1F("hCaloChargeTotal", "Charge in Calo;Charge (pC);# of events",100,0.2*gain,2.0*gain)
hCaloEdep = ROOT.TH1F("hCaloEdep", "Edep in Calo;Edep (GeV);# of events",100,0.0,1.8)

hCaloThetaX = ROOT.TH1F("hCaloThetaX", "Track #theta_{x} at Calo face;#theta_{x} (radians);# of tracks",100,-0.1,0.1)
hCaloThetaY = ROOT.TH1F("hCaloThetaY", "Track #theta_{y} at Calo face;#theta_{y} (radians);# of tracks",100,-0.1,0.1)

hTrackPvsPYdiff = ROOT.TProfile("hTrackPvsPYdiff" , "Track #Deltap_{Y}/p vs p;Track p (MeV);(pY_{upstream} - pY_{downstream}) / p_{total}",100, 1000.0, 200000.0)
hTrackPvsPXdiff = ROOT.TProfile("hTrackPvsPXdiff" , "Track #Deltap_{X}/p vs p;Track p (MeV);(pX_{upstream} - pX_{downstream}) / p_{total}",100, 1000.0, 200000.0)

#t.Print() # will show you all variables in ttree

i = 0
for event in t:
    i += 1

    if i%1000 == 0:
        print( "Processing event #%i of %i" % (i, t.GetEntries() ) )

    if event.longTracks > 0: # only process events with at least one track that has hits in last 3 tracking stations
        for j in range(event.longTracks): # loop over all long tracks in the event (long = has hits in last 3 tracking stations)
            if event.Track_p0[j] != 0.0:
                hTrackPvsPYdiff.Fill(event.Track_p0[j],(event.Track_py0[j] - event.Track_py1[j])/event.Track_p0[j])
                hTrackPvsPXdiff.Fill(event.Track_p0[j],(event.Track_px0[j] - event.Track_px1[j])/event.Track_p0[j])

            #print("track charge = %i and nLayers = %i" % (event.Track_charge[j],event.Track_nLayers[j]))
            #print("track upstream   (x,y,z) (px,py,pz) = (%f,%f,%f) (%f,%f,%f)" % (event.Track_x0[j],event.Track_y0[j],event.Track_z0[j],event.Track_px0[j],event.Track_py0[j],event.Track_pz0[j]))
            #print("track downstream (x,y,z) (px,py,pz) = (%f,%f,%f) (%f,%f,%f)" % (event.Track_x1[j],event.Track_y1[j],event.Track_z1[j],event.Track_px1[j],event.Track_py1[j],event.Track_pz1[j]))

            #print("track at vetoNu (x,y) (thetaX,thetaY) = (%f,%f) (%f,%f)" % (event.Track_X_atVetoNu[j],event.Track_Y_atVetoNu[j],event.Track_ThetaX_atVetoNu[j],event.Track_ThetaY_atVetoNu[j]))
            #print("track at Calo (x,y) (thetaX,thetaY) = (%f,%f) (%f,%f)" % (event.Track_X_atCalo[j],event.Track_Y_atCalo[j],event.Track_ThetaX_atCalo[j],event.Track_ThetaY_atCalo[j]))

        #print("number of track segments = ",event.TrackSegments)
        #for j in range(event.TrackSegments):
            #print("trackseg (x,y,z) (px,py,pz) = (%f,%f,%f) (%f,%f,%f)" % (event.TrackSegment_x[j],event.TrackSegment_y[j],event.TrackSegment_z[j],event.TrackSegment_px[j],event.TrackSegment_py[j],event.TrackSegment_pz[j]))
            #print("trackseg chi2 = %i and ndof = %i" % (event.TrackSegment_Chi2[j],event.TrackSegment_nDoF[j]))

        #print("number of SpacePoints = ",event.SpacePoints)
        #for j in range(event.SpacePoints):
            #print("Spacepoint #",j)
            #print("SpacePoint (x,y,z) = (%f,%f,%f)" % (event.SpacePoint_x[j],event.SpacePoint_y[j],event.SpacePoint_z[j]))

        hCaloEdep.Fill(event.Calo_total_Edep)
        hCaloChargeTotal.Fill(event.Calo_total_charge)

        x_calo = event.Track_X_atCalo[0]
        y_calo = event.Track_Y_atCalo[0]

        hCaloThetaX.Fill(event.Track_ThetaX_atCalo[0])
        hCaloThetaY.Fill(event.Track_ThetaY_atCalo[0])

        if abs(event.Track_ThetaX_atCalo[0]) > 0.1 or abs(event.Track_ThetaX_atCalo[0]) > 0.1: continue

        for chan,charge in enumerate([event.Calo0_raw_charge,event.Calo1_raw_charge,event.Calo2_raw_charge,event.Calo3_raw_charge]):
            if charge > 0.2*gain and charge < 2.0*gain:
                hXYvsEcalo[chan].Fill(x_calo,y_calo,charge)

        if x_calo > -60.0 and x_calo < -20.0 and y_calo > -80.0 and y_calo < -10.0:
            hCaloCharge[0].Fill(event.Calo0_raw_charge)
            hCaloPeak[0].Fill(event.Calo0_raw_peak)
        elif x_calo > 70.0 and x_calo < 100.0 and y_calo > -90.0 and y_calo < -10.0:
            hCaloCharge[1].Fill(event.Calo1_raw_charge)
            hCaloPeak[1].Fill(event.Calo1_raw_peak)
        elif x_calo > -60.0 and x_calo < -20.0 and y_calo > 20.0 and y_calo < 110.0:
            hCaloCharge[2].Fill(event.Calo2_raw_charge)
            hCaloPeak[2].Fill(event.Calo2_raw_peak)
        elif x_calo > 70.0 and x_calo < 100.0 and y_calo > 20.0 and y_calo < 110.0:
            hCaloCharge[3].Fill(event.Calo3_raw_charge)
            hCaloPeak[3].Fill(event.Calo3_raw_peak)

#    if i > 10000:
#        break

# create a list of histograms of random event integrals
hRandomCharge = []
for chan in range(15):
    hRandomCharge.append(rootFile.Get("hRandomCharge"+str(chan)))

# Now make some plots
filename = "analyze-"+runconfig+"-Ntuples.pdf"

c = ROOT.TCanvas()
c.Print(filename+'[')
hCaloEdep.Draw()
ROOT.gPad.SetLogy()
c.Print(filename)

c = ROOT.TCanvas()
hCaloChargeTotal.Draw()
c.Print(filename)

c = ROOT.TCanvas()
c.Divide(2,2)
for chan in range(4):
    c.cd(1+chan)
    hXYvsEcalo[chan].GetZaxis().SetRangeUser(hCaloCharge[chan].GetMean() - 0.3*hCaloCharge[chan].GetStdDev(),hCaloCharge[chan].GetMean() + 0.4*hCaloCharge[chan].GetStdDev())
    hXYvsEcalo[chan].Draw('COLZ')
c.Print(filename)

leg = []
c = ROOT.TCanvas()
c.Divide(2,2)
for chan in range(4):
    c.cd(1+chan)
    hCaloCharge[chan].Fit("landau")
    landgaus.SetParameters(hCaloCharge[chan].GetFunction("landau").GetParameter(0),hCaloCharge[chan].GetFunction("landau").GetParameter(1),hCaloCharge[chan].GetFunction("landau").GetParameter(2),0.0,hRandomCharge[chan].GetStdDev())
    landgaus.SetParLimits(0,0.1*hCaloCharge[chan].GetFunction("landau").GetParameter(0),20.0*hCaloCharge[chan].GetFunction("landau").GetParameter(0))
    landgaus.SetParLimits(1,0.5*hCaloCharge[chan].GetFunction("landau").GetParameter(1),1.2*hCaloCharge[chan].GetFunction("landau").GetParameter(1))
    landgaus.SetParLimits(2,0.1*hCaloCharge[chan].GetFunction("landau").GetParameter(2),1.2*hCaloCharge[chan].GetFunction("landau").GetParameter(2))
    landgaus.FixParameter(3,0.0)
    landgaus.FixParameter(4,hRandomCharge[chan].GetStdDev()) # fix gaussian smearing to the noise seen in randomly triggered events
    hCaloCharge[chan].Fit("landgaus","+")
    hCaloCharge[chan].GetFunction("landgaus").SetLineColor(4)
    hCaloCharge[chan].Draw()

    leg.append( ROOT.TLegend(0.55,0.55,0.89,0.75) )
    leg[chan].AddEntry(hCaloCharge[chan].GetFunction("landau"),"Landau MPV = "+str(hCaloCharge[chan].GetFunction("landau").GetParameter(1))[:6]+" #pm "+str(hCaloCharge[chan].GetFunction("landau").GetParError(1))[:6],"L")
    leg[chan].AddEntry(hCaloCharge[chan].GetFunction("landgaus"),"Landguas MPV = "+str(hCaloCharge[chan].GetFunction("landgaus").GetParameter(1))[:6]+" #pm "+str(hCaloCharge[chan].GetFunction("landgaus").GetParError(1))[:6],"L")
    leg[chan].AddEntry(hCaloCharge[chan].GetFunction("landgaus"),"Landguas gaussian width = "+str(hCaloCharge[chan].GetFunction("landgaus").GetParameter(4))[:6],"")
    leg[chan].SetBorderSize(0)
    leg[chan].Draw()
c.Print(filename)

leg = []
c = ROOT.TCanvas()
c.Divide(2,2)
for chan in range(4):
    c.cd(1+chan)
    hCaloPeak[chan].Fit("landau")
    hCaloPeak[chan].Draw()

    leg.append( ROOT.TLegend(0.55,0.55,0.89,0.75) )
    leg[chan].AddEntry(hCaloPeak[chan].GetFunction("landau"),"Landau MPV = "+str(hCaloPeak[chan].GetFunction("landau").GetParameter(1))[:6]+" #pm "+str(hCaloPeak[chan].GetFunction("landau").GetParError(1))[:6],"L")
    leg[chan].SetBorderSize(0)
    leg[chan].Draw()
c.Print(filename)

c = ROOT.TCanvas()
c.Divide(1,2)
c.cd(1)
hCaloThetaX.Draw()
c.cd(2)
hCaloThetaY.Draw()
c.Print(filename)

c = ROOT.TCanvas()
c.Divide(1,2)
c.cd(1)
hTrackPvsPYdiff.GetYaxis().SetRangeUser(hTrackPvsPYdiff.GetMean(2) - hTrackPvsPYdiff.GetStdDev(2), hTrackPvsPYdiff.GetMean(2) + hTrackPvsPYdiff.GetStdDev(2))
hTrackPvsPYdiff.Draw()
c.cd(2)
hTrackPvsPXdiff.GetYaxis().SetRangeUser(hTrackPvsPXdiff.GetMean(2) - hTrackPvsPXdiff.GetStdDev(2), hTrackPvsPXdiff.GetMean(2) + hTrackPvsPXdiff.GetStdDev(2))
hTrackPvsPXdiff.Draw()
c.Print(filename)

c = ROOT.TCanvas()
c.Divide(4,4)
for chan in range(15):
    c.cd(1+chan)
    hRandomCharge[chan].Draw()
c.Print(filename)

# Must close file at the end
c.Print(filename+']')

