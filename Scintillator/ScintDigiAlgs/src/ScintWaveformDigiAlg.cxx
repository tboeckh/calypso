#include "ScintWaveformDigiAlg.h"

#include "ScintSimEvent/ScintHitIdHelper.h"

#include <map>


ScintWaveformDigiAlg::ScintWaveformDigiAlg(const std::string& name, 
					 ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator) { 

}

StatusCode 
ScintWaveformDigiAlg::initialize() {
  ATH_MSG_INFO(name() << "::initalize()" );

  // Initalize tools
  ATH_CHECK( m_digiTool.retrieve() );
  ATH_CHECK( m_mappingTool.retrieve() );

  // Set key to read waveform from
  ATH_CHECK( m_scintHitContainerKey.initialize() );

  // Set key to write container
  ATH_CHECK( m_waveformContainerKey.initialize() );

  // Set up helpers
  ATH_CHECK(detStore()->retrieve(m_vetoID, "VetoID"));
  ATH_CHECK(detStore()->retrieve(m_vetoNuID, "VetoNuID"));
  ATH_CHECK(detStore()->retrieve(m_triggerID, "TriggerID"));
  ATH_CHECK(detStore()->retrieve(m_preshowerID, "PreshowerID"));

  // Create CB time kernel and pre-evaluate for number of samples
  m_kernel = new TF1("PDF", "[4] * ROOT::Math::crystalball_pdf(x, [0],[1],[2],[3])", 0, 1200);
  m_kernel->SetParameter(0, m_CB_alpha);
  m_kernel->SetParameter(1, m_CB_n);
  m_kernel->SetParameter(2, m_CB_sigma);
  m_kernel->SetParameter(3, m_CB_mean);
  m_kernel->SetParameter(4, m_CB_norm);

  // Pre-evaluate time kernel for each bin
  m_timekernel = m_digiTool->evaluate_timekernel(m_kernel);

  return StatusCode::SUCCESS;
}

StatusCode 
ScintWaveformDigiAlg::finalize() {
  ATH_MSG_INFO(name() << "::finalize()");

  delete m_kernel;

  return StatusCode::SUCCESS;
}

StatusCode 
ScintWaveformDigiAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("Executing");
  ATH_MSG_DEBUG("Run: " << ctx.eventID().run_number() << " Event: " << ctx.eventID().event_number());

  // Find the input HITS collection
  SG::ReadHandle<ScintHitCollection> scintHitHandle(m_scintHitContainerKey, ctx);

  ATH_CHECK( scintHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for ScintHitCollection " << m_scintHitContainerKey);

  // Find the output waveform container
  SG::WriteHandle<RawWaveformContainer> waveformContainerHandle(m_waveformContainerKey, ctx);
  ATH_CHECK( waveformContainerHandle.record( std::make_unique<RawWaveformContainer>()) );

  ATH_MSG_DEBUG("WaveformsContainer '" << waveformContainerHandle.name() << "' initialized");
  
  if (scintHitHandle->size() == 0) {
    ATH_MSG_DEBUG("ScintHitCollection found with zero length!");
    return StatusCode::SUCCESS;
  }
  
  // Create structure to store pulse for each channel
  std::map<Identifier, std::vector<uint16_t>> waveforms;

  auto first = *scintHitHandle->begin();
  if (first.isVeto()) {
    waveforms = m_digiTool->create_waveform_map(m_vetoID);
  } else if (first.isVetoNu()) {
    waveforms = m_digiTool->create_waveform_map(m_vetoNuID); 
  } else if (first.isTrigger()) {
    waveforms = m_digiTool->create_waveform_map(m_triggerID);
  } else if (first.isPreshower()) {
    waveforms = m_digiTool->create_waveform_map(m_preshowerID);
  } 

  // Loop over time samples
  // Should really do sums first, as repeating these in the loop is a waste
  for (const auto& tk : m_timekernel) {
    std::map<Identifier, float> counts;
   
    // Convolve hit energy with evaluated kernel and sum for each hit id (i.e. channel)
    Identifier id;
    for (const auto& hit : *scintHitHandle) {            

      // Special handling for trigger scintillator
      if (first.isTrigger()) {
	// Hits are created per plate
	// Need to split between 2 PMTs

	// Identifier from hit is plate ID
	Identifier plate_id = m_triggerID->plate_id(hit.getIdentifier());

	// Need to do something better here, but for now just fill both
	id = m_triggerID->pmt_id(plate_id, 0); // ID for PMT 0
	counts[id] += tk * hit.energyLoss();
	id = m_triggerID->pmt_id(plate_id, 1); // ID for PMT 1
	counts[id] += tk * hit.energyLoss();

      } else {
	// All others have only 1 PMT
	// Use detector id (not hit id) throughout
	id = hit.getIdentifier();
	counts[id] += tk * hit.energyLoss();
      }
    }

    // Subtract count from basleine and add result to correct waveform vector
    for (const auto& c : counts) {

      unsigned int baseline = m_digiTool->generate_baseline(m_base_mean, m_base_rms);
      int value = baseline - c.second;

      if (value < 0) {
	ATH_MSG_WARNING("Found pulse " << c.second << " larger than baseline " << c.first);
	value = 0; // Protect against scaling signal above baseline
      }

      // Convert hit id to Identifier and store 
      id = c.first;
      waveforms[id].push_back(value);
    }
  }

  // This is a bit of a hack to make sure all waveforms have
  // at least baseline entries.  Should really add this to the 
  // logic above
  for (const auto& w : waveforms) {
    if (w.second.size() > 0) continue;

    // Waveform was empty, fill with baseline
    int channel = m_mappingTool->getChannelMapping(w.first);
    ATH_MSG_DEBUG("Writing baseline into empty waveform in channel "<< channel);
    int i = m_digiTool->nsamples();
    while(i--) {  // Use while to avoid unused variable warning with for
      int baseline = m_digiTool->generate_baseline(m_base_mean, m_base_rms);
      waveforms[w.first].push_back(baseline);
    }
  }

  // Loop over wavefrom vectors to make and store raw waveform
  unsigned int nsamples = m_digiTool->nsamples();
  for (const auto& w : waveforms) {
    RawWaveform* wfm = new RawWaveform();
    wfm->setWaveform(0, w.second);
    wfm->setIdentifier(w.first);
    wfm->setSamples(nsamples);
    // Only save this waveform if channel mapping is valid
    // This will avoid making a waveform for the missing veto counter
    int channel = m_mappingTool->getChannelMapping(w.first);
    if (channel < 0) {
      ATH_MSG_DEBUG("Skipping waveform with unknown channel!");
      continue;
    }
    wfm->setChannel(channel);
    waveformContainerHandle->push_back(wfm);
  }

  ATH_MSG_DEBUG("WaveformsHitContainer " << waveformContainerHandle.name() << "' filled with "<< waveformContainerHandle->size() <<" items");

  return StatusCode::SUCCESS;
}
