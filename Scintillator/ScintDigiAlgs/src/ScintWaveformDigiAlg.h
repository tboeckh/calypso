#ifndef SCINTDIGIALGS_SCINTWAVEFORMDIGIALG_H
#define SCINTDIGIALGS_SCINTWAVEFORMDIGIALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Data classes
#include "WaveRawEvent/RawWaveformContainer.h"
#include "ScintSimEvent/ScintHitCollection.h"

// Tool classes
#include "WaveDigiTools/IWaveformDigitisationTool.h"
#include "WaveformConditionsTools/IWaveformCableMappingTool.h"

// Handles
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

//Helpers
#include "ScintIdentifier/VetoID.h"
#include "ScintIdentifier/VetoNuID.h"
#include "ScintIdentifier/TriggerID.h"
#include "ScintIdentifier/PreshowerID.h"
#include "ScintSimEvent/ScintHitIdHelper.h"
#include "Identifier/Identifier.h"


// ROOT
#include "TF1.h"


// STL
#include <string>
#include <vector>

class ScintWaveformDigiAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  ScintWaveformDigiAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~ScintWaveformDigiAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}


 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  ScintWaveformDigiAlg() = delete;
  ScintWaveformDigiAlg(const ScintWaveformDigiAlg&) = delete;
  ScintWaveformDigiAlg &operator=(const ScintWaveformDigiAlg&) = delete;
  //@}

  /// 

  /** @name Steerable pameters for crystal ball and baseline **/
  //@{
  Gaudi::Property<double> m_CB_alpha {this, "CB_alpha", 0, "Alpha of the crystal ball function"};
  Gaudi::Property<double> m_CB_n {this, "CB_n", 0, "n of the crystal ball function"};
  Gaudi::Property<double> m_CB_mean {this, "CB_mean", 0, "Mean of the crystal ball function"};  
  Gaudi::Property<double> m_CB_sigma {this, "CB_sigma", 0, "Sigma of the crystal ball function"};
  Gaudi::Property<double> m_CB_norm {this, "CB_norm", 0, "Norm of the crystal ball function"};

  Gaudi::Property<double> m_base_mean {this, "base_mean", 0, "Mean of the baseline"};
  Gaudi::Property<double> m_base_rms {this, "base_rms", 0, "RMS of the baseline"};
  //@}


  /** Kernel PDF and evaluated values **/
  //@{
  TF1*                            m_kernel;
  std::vector<float>              m_timekernel;
  //@}
  

  /// Detector ID helpers
  const VetoID* m_vetoID{nullptr};
  const VetoNuID* m_vetoNuID{nullptr};
  const TriggerID* m_triggerID{nullptr};
  const PreshowerID* m_preshowerID{nullptr};

  /**
   * @name Digitisation tool
   */
  ToolHandle<IWaveformDigitisationTool> m_digiTool
    {this, "WaveformDigitisationTool", "WaveformDigitisationTool"};

  /**
   * @name Mapping tool
   */
  ToolHandle<IWaveformCableMappingTool> m_mappingTool
    {this, "WaveformCableMappingTool", "WaveformCableMappingTool"};

  /**
   * @name Input HITS using SG::ReadHandleKey
   */
  //@{

  SG::ReadHandleKey<ScintHitCollection> m_scintHitContainerKey 
  {this, "ScintHitContainerKey", ""};

  //@}


  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<RawWaveformContainer> m_waveformContainerKey
    {this, "WaveformContainerKey", ""};
  //@}

};


#endif // SCINTDIGIALGS_SCINTDIGIALG_H
