/*
Copyright (C) 2022 CERN for the benefit of the FASER collaboration
*/

#pragma once

#include "GeoPrimitives/GeoPrimitives.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrkTrack/TrackCollection.h"

class FaserSCT_ID;
namespace  TrackerDD
{
    class SCT_DetectorManager;
}

namespace Tracker
{
    class PairVertexAlg : public AthReentrantAlgorithm 
    {
    public:
        PairVertexAlg(const std::string& name, ISvcLocator* pSvcLocator);

        virtual StatusCode initialize() override;
        virtual StatusCode execute(const EventContext& ctx) const override;
        virtual StatusCode finalize() override;

    private:
        double m_MeV2GeV = 1e-3;
        const FaserSCT_ID* m_idHelper {nullptr};
        const TrackerDD::SCT_DetectorManager* m_detMgr {nullptr};
        SG::ReadHandleKey<McEventCollection> m_mcEventCollectionKey { this, "McEventCollection", "TruthEvent" };

        SG::ReadHandleKey<TrackCollection> m_trackCollectionKey { this, "TrackCollection", "CKFTrackCollection" };

        ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool { this, "ExtrapolationTool", "FaserActsExtrapolationTool" };

        mutable std::atomic<size_t> m_numberOfEvents{0};
        mutable std::atomic<size_t> m_numberOfPositiveTracks{0};
        mutable std::atomic<size_t> m_numberOfNegativeTracks{0};
        mutable std::atomic<size_t> m_numberOfOppositeSignPairs{0};
        mutable std::atomic<size_t> m_numberOfNoParameterTracks{0};
        mutable std::atomic<size_t> m_invalidTrackContainerEntries{0};
        mutable std::atomic<size_t> m_numberOfNullParameters{0};
        mutable std::atomic<size_t> m_numberOfParametersWithoutSurface{0};
        mutable std::atomic<size_t> m_numberOfNoUpstreamTracks{0};
    };

}