atlas_subdir(MyExtrapolationExample)

atlas_add_component(
        MyExtrapolationExample
        src/MyExtrapolationExample.h
        src/MyExtrapolationExample.cxx
        src/components/MyExtrapolationExample_entries.cxx
        LINK_LIBRARIES AthenaBaseComps StoreGateLib GeneratorObjects FaserActsGeometryLib TrackerSimEvent TrackerIdentifier TrackerReadoutGeometry
)

atlas_install_python_modules(python/*.py)
atlas_install_scripts(test/*.py)
