#!/usr/bin/env python

import sys

if __name__ == "__main__":

    fileroot = "output"
    if len(sys.argv) > 1:
       filename = sys.argv[1]

    doRDO = False
    if len(sys.argv) > 2:
       filename = sys.argv[2]

    from AthenaCommon.Logging import log, logging
    from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaCommon.Configurable import Configurable
        
    Configurable.configurableRun3Behavior = True

    log.setLevel(VERBOSE)
    
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-FASER-03"             # Always needed; must match FaserVersion
    ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
    ConfigFlags.Input.ProjectName = "mc21"                       # Needed to bypass autoconfig
    ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
    ConfigFlags.GeoModel.FaserVersion     = "FASER-TB00"         # FASER geometry
    ConfigFlags.Common.isOnline = False
    ConfigFlags.GeoModel.Align.Dynamic = False
    
    ConfigFlags.Input.Files = [
        "/eos/project-f/faser-commissioning/Simulation/Test/TB.Elec.200GeV.SIM.root"
        ]

    if doRDO:
        ConfigFlags.Output.RDOFileName = f"{fileroot}.RDO.root"
    else:
        ConfigFlags.addFlag("Output.xAODFileName", f"{fileroot}.xAOD.root")
        ConfigFlags.Output.ESDFileName = f"{fileroot}.ESD.root"

    ConfigFlags.lock()

    from CalypsoConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(ConfigFlags)

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg

    acc.merge(PoolReadCfg(ConfigFlags))
    acc.merge(PoolWriteCfg(ConfigFlags))

    if doRDO:
         from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
         itemList = [
             "RawWaveformContainer#*"
             ]
         acc.merge(OutputStreamCfg(ConfigFlags, "RDO", itemList,disableEventTag=True))


    else:
        from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
        itemList = [
            "xAOD::EventInfo#*",
            "xAOD::WaveformHitContainer#*",
            "xAOD::WaveformHitAuxContainer#*",
            ]

        acc.merge(OutputStreamCfg(ConfigFlags, "xAOD", itemList, disableEventTag=True))

    from ScintDigiAlgs.ScintDigiAlgsConfig import ScintWaveformDigitizationCfg
    acc.merge(ScintWaveformDigitizationCfg(ConfigFlags))

    from CaloDigiAlgs.CaloDigiAlgsConfig import CaloWaveformDigitizationCfg
    acc.merge(CaloWaveformDigitizationCfg(ConfigFlags))

    if not doRDO:
        from WaveRecAlgs.WaveRecAlgsConfig import WaveformReconstructionCfg
        acc.merge(WaveformReconstructionCfg(ConfigFlags))

    #acc.foreach_component("*").OutputLevel = VERBOSE

    # Execute and finish
    sc = acc.run(maxEvents=100)

    # Success should be 0
    sys.exit(not sc.isSuccess())
